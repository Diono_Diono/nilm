#include <FS.h>
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

//needed for library
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include <WiFiManager.h>          //https://github.com/kentaylor/WiFiManager

#include <DoubleResetDetector.h>  //https://github.com/datacute/DoubleResetDetector

#include <ModbusIP_ESP8266.h> //https://github.com/vermut/arduino-ModbusIP_ESP8266
#include <ModbusMaster.h>
#include <SoftwareSerial.h>
#include <Ticker.h>

// Number of seconds after reset during which a 
// subseqent reset will be considered a double reset.
#define DRD_TIMEOUT 10

// RTC Memory Address for the DoubleResetDetector to use
#define DRD_ADDRESS 0

#define ON LOW
#define OFF HIGH
#define POLLING     1000

// D0 (16)
// D1 (5)
// D2 (4)
// D3 (0)
// D4 (2)
// D5 (14)
// D6 (12)
// D7 (13)
// D8 (15)
/*!
  We're using a MAX485-compatible RS485 Transceiver.
  Rx/Tx is hooked up to the hardware serial port at 'Serial'.
  The Data Enable and Receiver Enable pins are hooked up as follows:
*/
#define MAX485_DE      12         // DE and /RE are tied together
#define TXD            14
#define RXD            13
//#define PZEM
#define SPM91

#define CONFIG        ticker.detach(); ticker.attach_ms(10, flip, 50);
#define CONNECTED     ticker.detach(); ticker.attach_ms(10, flip, 1);

DoubleResetDetector drd(DRD_TIMEOUT, DRD_ADDRESS);
ESP8266WebServer server(80);
//ModbusIP object
ModbusIP mb;
// instantiate ModbusMaster object
ModbusMaster slave;

SoftwareSerial swSer(RXD, TXD, false, 256);   //(rx, tx, inv, buf_size)
Ticker ticker;



// Constants

// select wich pin will trigger the configuraton portal when set to LOW
// ESP-01 users please note: the only pins available (0 and 2), are shared 
// with the bootloader, so always set them HIGH at power-up
// Onboard LED I/O pin on NodeMCU board
const int PIN_LED = 2; // D4 on NodeMCU and WeMos. Controls the onboard LED.
/* Trigger for inititating config mode is Pin D3 and also flash button on NodeMCU
 * Flash button is convenient to use but if it is pressed it will stuff up the serial port device driver 
 * until the computer is rebooted on windows machines.
 */     
const int TRIGGER_PIN = 0; // D3 on NodeMCU and WeMos.
/*
 * Alternative trigger pin. Needs to be connected to a button to use this pin. It must be a momentary connection
 * not connected permanently to ground. Either trigger pin will work.
 */
const int TRIGGER_PIN2 = 0; // D7 on NodeMCU and WeMos.

const char* CONFIG_FILE = "/config.json";

// Variables

// Indicates whether ESP has WiFi credentials saved from previous session
bool initialConfig = false;

// Default configuration values
char thingspeakApiKey[17] = "";
bool sensorDht22 = true;
unsigned int mbID = 1;
unsigned int mbFC = 3;
unsigned int mbStart = 0;
unsigned int mbLen = 1;
uint32_t     mbReadCount = 0;
uint32_t     mbFail = 0;
long ts;
uint16_t     duration = 100;
#if defined(PZEM)
#define LEN   9
#define ID    1
#define FC    4

//Modbus Registers Offsets (0-9999)
const int VOLTAGE_IREG = 100;
const int CURRENT_L_IREG = VOLTAGE_IREG + 1;
const int CURRENT_H_IREG = VOLTAGE_IREG + 2;
const int POWER_L_IREG   = VOLTAGE_IREG + 3;
const int POWER_H_IREG   = VOLTAGE_IREG + 4;
const int KWH_L_IREG     = VOLTAGE_IREG + 5;
const int KWH_H_IREG     = VOLTAGE_IREG + 6;
const int FREQ_IREG      = VOLTAGE_IREG + 7;
const int PF_IREG        = VOLTAGE_IREG + 8;
#endif

#if defined(SPM91)
#define LEN   28
#define ID    15
#define FC    3
//Modbus Registers Offsets (0-9999)
const int VOLTAGE_IREG   = 100;
const int CURRENT_L_IREG = VOLTAGE_IREG + 1;
const int CURRENT_H_IREG = VOLTAGE_IREG + 2;
const int W_L_IREG       = VOLTAGE_IREG + 3;
const int W_H_IREG       = VOLTAGE_IREG + 4;
const int VA_L_IREG      = VOLTAGE_IREG + 5;
const int VA_H_IREG      = VOLTAGE_IREG + 6;
const int VAR_L_IREG     = VOLTAGE_IREG + 7;
const int VAR_H_IREG     = VOLTAGE_IREG + 8;
const int FREQ_IREG      = VOLTAGE_IREG + 9;
const int PF_IREG        = VOLTAGE_IREG + 10;

const int KWH_L_IREG     = VOLTAGE_IREG + 11;
const int KWH_H_IREG     = VOLTAGE_IREG + 12;

#endif


// Function Prototypes

bool readConfigFile();
bool writeConfigFile();

void TaskMBRead( void *pvParameters );


void flip(int state) {
  uint16_t offTime;
  uint16_t onTime;
  static uint16_t count;
  offTime = state;
  onTime = duration - state;
  if (count <= offTime)
    digitalWrite(LED_BUILTIN, OFF);
  if ((count >= onTime) && (count <duration)) {
    digitalWrite(LED_BUILTIN, ON);
  }
  count++;
  if (count > duration) {
    count = 0;
    digitalWrite(LED_BUILTIN, OFF);
  }
}

void preTransmission()
{
  digitalWrite(MAX485_DE, 1);
}

void postTransmission()
{
  digitalWrite(MAX485_DE, 0);
}

void idleTask() {
  mb.task();
}

void handleRoot() {
  digitalWrite(PIN_LED, ON);
  String message = "Config: \n\n";
#if defined(PZEM)
  message += "Device: PZEM\n";
#endif
#if defined(SPM91)
  message += "Device: SPM91\n";
#endif
  
  message += "Modbus ID: ";
  message += String(mbID)+"\n";
  message += "Modbus Function Code: ";
  message += String(mbFC)+"\n";
  message += "Modbus Start Register: ";
  message += String(mbStart)+"\n";
  message += "Modbus Register Count: ";
  message += String(mbLen)+"\n";
  message += "Modbus Read Count: ";
  message += String(mbReadCount)+"\n";
  message += "Modbus Read Failed: ";
  message += String(mbFail)+"\n";
  
  
  
  server.send(200, "text/plain", message);
  digitalWrite(PIN_LED, OFF);
}

void handleNotFound() {
  digitalWrite(PIN_LED, ON);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(PIN_LED, OFF);
}

bool wifiFirstConnected;
void onSTAConnected (WiFiEventStationModeConnected ipInfo) {
    Serial.printf ("Connected to %s\r\n", ipInfo.ssid.c_str ());
}

// IP network is connected
void onSTAGotIP (WiFiEventStationModeGotIP ipInfo) {
    Serial.printf ("Got IP: %s\r\n", ipInfo.ip.toString ().c_str ());
    Serial.printf ("Connected: %s\r\n", WiFi.status () == WL_CONNECTED ? "yes" : "no");
//    digitalWrite (PIN_LED, LOW); // Turn on LED
    wifiFirstConnected = true;
//    duration = 200;
    ticker.detach(); 
    digitalWrite (PIN_LED, OFF); // Turn on LED
    ticker.attach_ms(10, flip, 1);
}

// Manage network disconnection
void onSTADisconnected (WiFiEventStationModeDisconnected event_info) {
    Serial.printf ("Disconnected from SSID: %s\n", event_info.ssid.c_str ());
    Serial.printf ("Reason: %d\n", event_info.reason);
    digitalWrite (PIN_LED, HIGH); // Turn off LED
    wifiFirstConnected = false;
}

// Setup function
void setup() {
  static WiFiEventHandler e1, e2, e3;
  // Put your setup code here, to run once
  Serial.begin(115200);
  Serial.println("\n Starting");
  CONFIG

  // Initialize the LED digital pin as an output.
  pinMode(PIN_LED, OUTPUT);
  digitalWrite(PIN_LED, OFF);
  // Initialize trigger pins
  pinMode(TRIGGER_PIN, INPUT_PULLUP);
  pinMode(TRIGGER_PIN2, INPUT_PULLUP);

  pinMode(MAX485_DE, OUTPUT);
  // Init in receive mode
  digitalWrite(MAX485_DE, 0);

  mbID = ID;
  mbFC = FC;
  mbStart = 0;
  mbLen = LEN;

  swSer.begin(9600); 
  
  // Mount the filesystem
  bool result = SPIFFS.begin();
  Serial.println("SPIFFS opened: " + result);

  if (!readConfigFile()) {
    Serial.println("Failed to read configuration file, using default values");
    writeConfigFile();
  }

  WiFi.printDiag(Serial); //Remove this line if you do not want to see WiFi password printed
  e1 = WiFi.onStationModeGotIP (onSTAGotIP);// As soon WiFi is connected, start NTP Client
  e2 = WiFi.onStationModeDisconnected (onSTADisconnected);
  e3 = WiFi.onStationModeConnected (onSTAConnected);    
  if (WiFi.SSID() == "") {
    Serial.println("We haven't got any access point credentials, so get them now");
    initialConfig = true;
  } 
  if (drd.detectDoubleReset()) {
    Serial.println("Double Reset Detected");
    initialConfig = true;
  }
  wifiConfig();
  
//  digitalWrite(PIN_LED, OFF); // Turn LED off as we are not in configuration mode.
  WiFi.mode(WIFI_STA); // Force to station mode because if device was switched off while in access point mode it will start up next time in access point mode.
  unsigned long startedAt = millis();
  Serial.print("After waiting ");
  int connRes = WiFi.waitForConnectResult();
  float waited = (millis()- startedAt);
  Serial.print(waited/1000);
  Serial.print(" secs in setup() connection result is ");
  Serial.println(connRes);


  if (WiFi.status()!=WL_CONNECTED){
    Serial.println("Failed to connect, finishing setup anyway");
  } else{
    Serial.print("Local ip: ");
    Serial.println(WiFi.localIP());
    duration = 200;
    ticker.detach(); 
    digitalWrite (PIN_LED, OFF); // Turn on LED
    ticker.attach_ms(10, flip, 1);
    server.on("/", handleRoot);

    server.on("/inline", []() {
      server.send(200, "text/plain", "this works as well");
    });

    server.onNotFound(handleNotFound);

    server.begin();
    Serial.println("HTTP server started");    

    slave.begin(mbID, swSer);
    // Callbacks allow us to configure the RS485 transceiver correctly
    slave.preTransmission(preTransmission);
    slave.postTransmission(postTransmission);
    slave.idle(idleTask);

     mb.begin();    //Start Modbus IP
    // Add SENSOR_IREG register - Use addIreg() for analog Inputs

#if defined(PZEM)
    mb.addIreg(VOLTAGE_IREG);
    mb.addIreg(CURRENT_L_IREG);
    mb.addIreg(CURRENT_H_IREG);

    mb.addIreg(POWER_L_IREG);
    mb.addIreg(POWER_H_IREG);
    mb.addIreg(KWH_L_IREG);
    mb.addIreg(KWH_H_IREG);
    mb.addIreg(FREQ_IREG);
    mb.addIreg(PF_IREG);

    mb.Ireg(VOLTAGE_IREG,   0);
    mb.Ireg(CURRENT_L_IREG, 0);
    mb.Ireg(CURRENT_H_IREG, 0);
 
    mb.Ireg(POWER_L_IREG,   0);
    mb.Ireg(POWER_H_IREG,   0);
    mb.Ireg(KWH_L_IREG,     0);
    mb.Ireg(KWH_H_IREG,     0);
    mb.Ireg(FREQ_IREG,      0);
    mb.Ireg(PF_IREG,        0);         
#endif

#if defined(SPM91)
    mb.addIreg(VOLTAGE_IREG);
    mb.addIreg(CURRENT_L_IREG);
    mb.addIreg(CURRENT_H_IREG);
    mb.addIreg(W_H_IREG);
    mb.addIreg(W_L_IREG);
    mb.addIreg(VA_H_IREG);
    mb.addIreg(VA_L_IREG);
    mb.addIreg(VAR_H_IREG);
    mb.addIreg(VAR_L_IREG);
    mb.addIreg(FREQ_IREG);
    mb.addIreg(PF_IREG);

    mb.addIreg(KWH_H_IREG);
    mb.addIreg(KWH_L_IREG);

    mb.Ireg(VOLTAGE_IREG, 0); 
    mb.Ireg(CURRENT_L_IREG, 0);
    mb.Ireg(CURRENT_H_IREG, 0);
    mb.Ireg(W_H_IREG, 0);
    mb.Ireg(W_L_IREG, 0);
    mb.Ireg(VA_H_IREG, 0);
    mb.Ireg(VA_L_IREG, 0);
    mb.Ireg(VAR_H_IREG, 0);
    mb.Ireg(VAR_L_IREG, 0);
    mb.Ireg(FREQ_IREG, 0);
    mb.Ireg(PF_IREG, 0);

    mb.Ireg(KWH_H_IREG, 0);
    mb.Ireg(KWH_L_IREG, 0);
       

#endif    

    ts = millis();
    
  }
}

void wifiConfig() {
    // is configuration portal requested?
  if ((digitalRead(TRIGGER_PIN) == LOW) || (digitalRead(TRIGGER_PIN2) == LOW) || (initialConfig)) {
     Serial.println("Configuration portal requested");
     digitalWrite(PIN_LED, ON); // turn the LED on by making the voltage LOW to tell us we are in configuration mode.
    //Local intialization. Once its business is done, there is no need to keep it around

    // Extra parameters to be configured
    // After connecting, parameter.getValue() will get you the configured value
    // Format: <ID> <Placeholder text> <default value> <length> <custom HTML> <label placement>

    // Thingspeak API Key - this is a straight forward string parameter
//    WiFiManagerParameter p_thingspeakApiKey("thingspeakapikey", "Thingspeak API Key", thingspeakApiKey, 17);

    // DHT-22 sensor present or not - bool parameter visualized using checkbox, so couple of things to note
    // - value is always 'T' for true. When the HTML form is submitted this is the value that will be 
    //   sent as a parameter. When unchecked, nothing will be sent by the HTML standard.
    // - customhtml must be 'type="checkbox"' for obvious reasons. When the default is checked
    //   append 'checked' too
    // - labelplacement parameter is WFM_LABEL_AFTER for checkboxes as label has to be placed after the input field

//    char customhtml[24] = "type=\"checkbox\"";
//    if (sensorDht22) {
//      strcat(customhtml, " checked");
//    }
//    WiFiManagerParameter p_sensorDht22("sensordht22", "DHT-22 Sensor", "T", 2, customhtml, WFM_LABEL_AFTER);

    // I2C SCL and SDA parameters are integers so we need to convert them to char array but
    // no other special considerations
    char convertedValue[3];
//    sprintf(convertedValue, "%d", pinSda);
//    WiFiManagerParameter p_pinSda("pinsda", "I2C SDA pin", convertedValue, 3);
//    sprintf(convertedValue, "%d", pinScl);
//    WiFiManagerParameter p_pinScl("pinscl", "I2C SCL pin", convertedValue, 3);

    sprintf(convertedValue, "%d", mbID);
    WiFiManagerParameter p_mbID("mbID", "Modbus Device ID", convertedValue, 3);
    sprintf(convertedValue, "%d", mbFC);
    WiFiManagerParameter p_mbFC("mbFC", "Modbus Device Function Code", convertedValue, 3);
    sprintf(convertedValue, "%d", mbStart);
    WiFiManagerParameter p_mbStart("mbStart", "Modbus Device Start Reg", convertedValue, 3);
    sprintf(convertedValue, "%d", mbLen);
    WiFiManagerParameter p_mbLen("mbLen", "Modbus Device Reg Count", convertedValue, 3);


    // Just a quick hint
    WiFiManagerParameter p_hint("<small>*Hint: if you want to reuse the currently active WiFi credentials, leave SSID and Password fields empty</small>");
    
    // Initialize WiFIManager
    WiFiManager wifiManager;
    
    //add all parameters here
    
    wifiManager.addParameter(&p_hint);
    wifiManager.addParameter(&p_mbID);
    wifiManager.addParameter(&p_mbFC);
    wifiManager.addParameter(&p_mbStart);
    wifiManager.addParameter(&p_mbLen);  

    // Sets timeout in seconds until configuration portal gets turned off.
    // If not specified device will remain in configuration mode until
    // switched off via webserver or device is restarted.
    // wifiManager.setConfigPortalTimeout(600);

    // It starts an access point 
    // and goes into a blocking loop awaiting configuration.
    // Once the user leaves the portal with the exit button
    // processing will continue
    if (!wifiManager.startConfigPortal()) {
      Serial.println("Not connected to WiFi but continuing anyway.");
    } else {
      // If you get here you have connected to the WiFi
      Serial.println("Connected...yeey :)");
    }


    // Getting posted form values and overriding local variables parameters
    // Config file is written regardless the connection state
//    strcpy(thingspeakApiKey, p_thingspeakApiKey.getValue());
//    sensorDht22 = (strncmp(p_sensorDht22.getValue(), "T", 1) == 0);
//    pinSda = atoi(p_pinSda.getValue());
//    pinScl = atoi(p_pinScl.getValue());
    mbID    = atoi(p_mbID.getValue());
    mbFC    = atoi(p_mbFC.getValue());
    mbStart = atoi(p_mbStart.getValue());
    mbLen   = atoi(p_mbLen.getValue());
    
    // Writing JSON config file to flash for next boot
    writeConfigFile();

    
    digitalWrite(PIN_LED, OFF); // Turn LED off as we are not in configuration mode.

////    ESP.reset(); // This is a bit crude. For some unknown reason webserver can only be started once per boot up 
    // so resetting the device allows to go back into config mode again when it reboots.
    delay(5000);
  }

}
// Loop function

void loop() {
  uint8_t result;
  drd.loop();
  // Configuration portal not requested, so run normal loop
  // Put your main code here, to run repeatedly...
  server.handleClient();

  //Call once inside loop() - all magic here
  mb.task();

   //Read each two seconds
   if (millis() > ts + POLLING) {
       ts = millis();
       
       Serial.println("\nSending req to meter");
#if defined(PZEM)       
       mbReadCount++;
       // Read 9 registers starting at 0x0)
       if (mbFC == 3) 
         result = slave.readHoldingRegisters(mbStart, mbLen);        // Read REGs from mbStart
       else if (mbFC ==4) 
         result = slave.readInputRegisters(mbStart, mbLen);        // Read REGs from mbStart

       if (result == slave.ku8MBSuccess)  {

         Serial.println("\nUpdate Register Values...");

         mb.Ireg(VOLTAGE_IREG,   slave.getResponseBuffer(0));
         mb.Ireg(CURRENT_L_IREG, slave.getResponseBuffer(1));
         mb.Ireg(CURRENT_H_IREG, slave.getResponseBuffer(2));
 
         mb.Ireg(POWER_L_IREG,   slave.getResponseBuffer(3));
         mb.Ireg(POWER_H_IREG,   slave.getResponseBuffer(4));
         mb.Ireg(KWH_L_IREG,     slave.getResponseBuffer(5));
         mb.Ireg(KWH_H_IREG,     slave.getResponseBuffer(6));
         mb.Ireg(FREQ_IREG,      slave.getResponseBuffer(7));
         mb.Ireg(PF_IREG,        slave.getResponseBuffer(8));         
       } else {
         mbFail++;
       }
#endif       

#if defined (SPM91)
       mbReadCount++;
       // Read 9 registers starting at 0x0)
       if (mbFC == 3) 
         result = slave.readHoldingRegisters(mbStart, mbLen);        // Read REGs from mbStart
       else if (mbFC ==4) 
         result = slave.readInputRegisters(mbStart, mbLen);        // Read REGs from mbStart
       
       if (result == slave.ku8MBSuccess)  {
         Serial.println("\nUpdate Register Values...");
         Serial.printf("\t%d\n", slave.getResponseBuffer(0));
         Serial.printf("\t%d\n", slave.getResponseBuffer(1));
         Serial.printf("\t%d\n", slave.getResponseBuffer(2));
         Serial.printf("\t%d\n", slave.getResponseBuffer(3));
         Serial.printf("\t%d\n", slave.getResponseBuffer(4));
         
         mb.Ireg(VOLTAGE_IREG,   slave.getResponseBuffer(2));
         mb.Ireg(CURRENT_L_IREG, slave.getResponseBuffer(3));
         mb.Ireg(CURRENT_H_IREG, slave.getResponseBuffer(4));
         mb.Ireg(W_L_IREG, slave.getResponseBuffer(5));
         mb.Ireg(W_H_IREG, slave.getResponseBuffer(6));
         mb.Ireg(VA_L_IREG, slave.getResponseBuffer(7));
         mb.Ireg(VA_H_IREG, slave.getResponseBuffer(8));
         mb.Ireg(VAR_L_IREG, slave.getResponseBuffer(9));
         mb.Ireg(VAR_H_IREG, slave.getResponseBuffer(10));
         mb.Ireg(FREQ_IREG, slave.getResponseBuffer(11));
         mb.Ireg(PF_IREG, slave.getResponseBuffer(12));

         mb.Ireg(KWH_L_IREG, slave.getResponseBuffer(0));
         mb.Ireg(KWH_H_IREG, slave.getResponseBuffer(1));
       } else {
         mbFail++;
       }

#endif
   }
   delay(10);  
}

bool readConfigFile() {
  // this opens the config file in read-mode
  File f = SPIFFS.open(CONFIG_FILE, "r");
  
  if (!f) {
    Serial.println("Configuration file not found");
    return false;
  } else {
    // we could open the file
    size_t size = f.size();
    Serial.printf("Config File size: %d\n", size);
    // Allocate a buffer to store contents of the file.
    std::unique_ptr<char[]> buf(new char[size]);

    // Read and store file contents in buf
    f.readBytes(buf.get(), size);
    // Closing file
    f.close();
    // Using dynamic JSON buffer which is not the recommended memory model, but anyway
    // See https://github.com/bblanchon/ArduinoJson/wiki/Memory%20model
    DynamicJsonDocument jsonBuffer;
    // Parse JSON string
    JsonObject& json = jsonBuffer.parseObject(buf.get());
    // Test if parsing succeeds.
    if (!json.success()) {
      Serial.println("JSON parseObject() failed");
      return false;
    }
    json.printTo(Serial);

    // Parse all config file parameters, override 
    // local config variables with parsed values
    
    if (json.containsKey("Modbus_ID")) {
      mbID = json["Modbus_ID"];
    }
    if (json.containsKey("Modbus_FC")) {
      mbFC = json["Modbus_FC"];
    }
    if (json.containsKey("Modbus_Start")) {
      mbStart = json["Modbus_Start"];
    }
    if (json.containsKey("Modbus_Len")) {
      mbLen = json["Modbus_Len"];
    }
  }
  Serial.println("\nConfig file was successfully parsed");
  return true;
}

bool writeConfigFile() {
  Serial.println("Saving config file");
  DynamicJsonBuffer jsonBuffer;
  DynamicJsonDocument jsonDocument;
  JsonObject& json = jsonBuffer.createObject();
  JsonObject& json = jsonDocument.createObject();

  // JSONify local configuration parameters
  json["Modbus_ID"]    = mbID;
  json["Modbus_FC"]    = mbFC;
  json["Modbus_Start"] = mbStart;
  json["Modbus_Len"]   = mbLen;

  // Open file for writing
  File f = SPIFFS.open(CONFIG_FILE, "w");
  if (!f) {
    Serial.println("Failed to open config file for writing");
    return false;
  }

  json.prettyPrintTo(Serial);
  // Write data to file and close it
  json.printTo(f);
  f.close();

  Serial.println("\nConfig file was successfully saved");
  return true;
}
