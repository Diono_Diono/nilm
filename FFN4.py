import numpy as np
import pandas as pd
import tensorflow as tf
from keras.models import Model, Sequential
from keras.layers import Input, Activation, Dense
from keras.optimizers import SGD
from keras.utils.np_utils import to_categorical



# Correlation Matrix for target
# corr_matrix = sf_train.corr()
# print(corr_matrix['type'])

# Drop unnecessary columns
# sf_train.drop(sf_train.columns[[5, 12, 14, 21, 22, 23]], axis=1, inplace=True)
# print(sf_train.head())

# Pandas read Validation CSV
# sf_val = pd.read_csv('p5_val_data.csv')

# Drop unnecessary columns
# sf_val.drop(sf_val.columns[[5, 12, 14, 21, 22, 23]], axis=1, inplace=True)
# Pandas read CSV

sf_train = pd.read_csv('3Power.csv')
# Get Pandas array value (Convert to NumPy array)
train_data = sf_train.values
# val_data = sf_val.values

# Use columns 2 to last as Input
train_x = train_data[:, 2:]
# val_x = val_data[:,2:]
# Use columns 1 as Output/Target (One-Hot Encoding)
train_y = to_categorical(train_data[:, 1])
# val_y = to_categorical( val_data[:,1] )

# Create Network
inputs = Input(shape=(1001,))
h_layer = Dense(10, activation='sigmoid')(inputs)
# Softmax Activation for Multiclass Classification
outputs = Dense(3, activation='softmax')(h_layer)
model = Model(inputs=inputs, outputs=outputs)

# Optimizer / Update Rule
sgd = SGD(lr=0.001)
# Compile the model with Cross Entropy Loss
model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy'])
# Train the model and use validation data
model.fit(train_x, train_y, batch_size=1001, epochs=5000, verbose=1)  # , validation_data=(val_x, val_y))
model.save('weights_power.h5')

# Predict all Validation data
predict = model.predict([214])

# Visualize Prediction
df = pd.DataFrame(predict)
df.columns = ['Light', 'Steamer', 'Steamer']
# df.index = val_data[:,0]
print(df)