import numpy as np
from keras.models import Sequential
from keras.layers import Dense
import pandas as pd
import matplotlib.pyplot as plt
from keras.layers import Dropout
#np.set_printoptions(threshold=np.inf)

# Data processing =======================================================
sf_train = pd.read_csv('6appK.csv')
#print (sf_train)
df=pd.DataFrame(sf_train,columns=['Light','Fans','Steamer_L','Chroma','Dryer_L','Steamer_H','Oven','Dryer_H'])
Light=df['Light']
Fans=df['Fans']
Steamer_L=df['Steamer_L']
Chroma=df['Chroma']
Dryer_L=df['Dryer_L']
Steamer_H=df['Steamer_H']
Oven=df['Oven']
Dryer_H=df['Dryer_H']


Light=np.array(Light)
Light=np.reshape(Light,(1000,1))
Light=Light.astype(np.float64)

Fans=np.array(Fans)
Fans=np.reshape(Fans,(1000,1))
Fans=Fans.astype(np.float64)

Steamer_L=np.array(Steamer_L)
Steamer_L=np.reshape(Steamer_L,(1000,1))
Steamer_L=Steamer_L.astype(np.float64)

Chroma=np.array(Chroma)
Chroma=np.reshape(Chroma,(1000,1))
Chroma=Chroma.astype(np.float64)

Dryer_L=np.array(Dryer_L)
Dryer_L=np.reshape(Dryer_L,(1000,1))
Dryer_L=Dryer_L.astype(np.float64)

Steamer_H=np.array(Steamer_H)
Steamer_H=np.reshape(Steamer_H,(1000,1))
Steamer_H=Steamer_H.astype(np.float64)

Oven=np.array(Oven)
Oven=np.reshape(Oven,(1000,1))
Oven=Oven.astype(np.float64)

Dryer_H=np.array(Dryer_H)
Dryer_H=np.reshape(Dryer_H,(1000,1))
Dryer_H=Dryer_H.astype(np.float64)

L0=[]
for i in range(len(Light)):
    if Light[i]==0:
        L0.append(i)
Light=np.delete(Light,L0)

a0=[]
for i in range(len(Fans)):
    if Fans[i]==0:
        a0.append(i)
Fans=np.delete(Fans,a0)

S0=[]
for i in range(len(Steamer_L)):
    if Steamer_L[i]==0:
        S0.append(i)
Steamer_L=np.delete(Steamer_L,S0)

C0=[]
for i in range(len(Chroma)):
    if Chroma[i]==0:
        C0.append(i)
Chroma=np.delete(Chroma,C0)

D0=[]
for i in range(len(Dryer_L)):
    if Dryer_L[i]==0:
        D0.append(i)
Dryer_L=np.delete(Dryer_L,D0)

S1=[]
for i in range(len(Steamer_H)):
    if Steamer_H[i]==0:
        S1.append(i)
Steamer_H=np.delete(Steamer_H,S1)

O0=[]
for i in range(len(Oven)):
    if Oven[i]==0:
        O0.append(i)
Oven=np.delete(Oven,O0)

D1=[]
for i in range(len(Dryer_H)):
    if Dryer_H[i]==0:
        D1.append(i)
Dryer_H=np.delete(Dryer_H,D1)

dataset_L=[]
dataset_F=[]
dataset_SL=[]
dataset_C=[]
dataset_DL=[]
dataset_SH=[]
dataset_O=[]
dataset_DH=[]

for i in range(len(Light)):
    dataset_L.append([Light[i],np.array([1,0,0,0,0,0,0,0])])

for i in range(len(Fans)):
    dataset_F.append([Fans[i], np.array([0,1,0,0,0,0,0,0])])

for i in range(len(Steamer_L)):
    dataset_SL.append([Steamer_L[i], np.array([0,0,1,0,0,0,0,0])])

for i in range(len(Chroma)):
    dataset_C.append([Chroma[i], np.array([0,0,0,1,0,0,0,0])])

for i in range(len(Dryer_L)):
    dataset_DL.append([Dryer_L[i], np.array([0,0,0,0,1,0,0,0])])

for i in range(len(Steamer_H)):
    dataset_SH.append([Steamer_H[i], np.array([0,0,0,0,0,1,0,0])])

for i in range(len(Oven)):
    dataset_O.append([Oven[i], np.array([0,0,0,0,0,0,1,0])])

for i in range(len(Dryer_H)):
    dataset_DH.append([Dryer_H[i], np.array([0,0,0,0,0,0,0,1])])
#print(dataset)
np.random.shuffle(dataset_L+dataset_F+dataset_SL+dataset_C+dataset_DL+dataset_SH+dataset_O+dataset_DH)
#print(np.random.shuffle)
data, label = zip(*dataset_L+dataset_F+dataset_SL+dataset_C+dataset_DL+dataset_SH+dataset_O+dataset_DH)
print(data,label)

data = np.array(data)
data=np.reshape(data,(8000,1))
#print(data)
label = np.array(label)
#print(label)

np.savez('6appK1.npz',data=data,label=label)
# ============================================================
