import numpy as np
from keras.models import Sequential
from keras.layers import Dense
import pandas as pd
import matplotlib.pyplot as plt
from keras.layers import Dropout
np.set_printoptions(threshold=np.inf)

# Data processing =======================================================
sf_train = pd.read_csv('3Power_New_bulb.csv')

df=pd.DataFrame(sf_train,columns=['Light','Fans','Steamer'])
Light=df['Light']
Fans=df['Fans']
Steamer=df['Steamer']

Light=np.array(Light)
Light=np.reshape(Light,(1001,1))
Light=Light.astype(np.float64)
Fans=np.array(Fans)
Fans=np.reshape(Fans,(1001,1))
Fans=Fans.astype(np.float64)
Steamer=np.array(Steamer)
Steamer=np.reshape(Steamer,(1001,1))
Steamer=Steamer.astype(np.float64)

L0=[]
for i in range(len(Light)):
    if Light[i]==0:
        L0.append(i)
Light=np.delete(Light,L0)
a0=[]
for i in range(len(Fans)):
    if Fans[i]==0:
        a0.append(i)
Fans=np.delete(Fans,a0)
S0=[]
for i in range(len(Steamer)):
    if Steamer[i]==0:
        S0.append(i)
Steamer=np.delete(Steamer,S0)

dataset=[]
for i in range(len(Light)):
    dataset.append([Light[i],np.array([1,0,0])])
for i in range(len(Fans)):
    dataset.append([Fans[i], np.array([0,1,0])])
for i in range(len(Steamer)):
    dataset.append([Steamer[i], np.array([0,0,1])])
#print(dataset)
np.random.shuffle(dataset)
data, label = zip(*dataset)
#print(data,label)
data = np.array(data)
data=np.reshape(data,(2781,1))
print(data)
label = np.array(label)

np.savez('Data_power_bulb.npz',data=data,label=label)
# ============================================================
