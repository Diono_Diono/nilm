import requests

url = "http://192.168.0.9:8086/query"

querystring = {"u":"Dion","p":"Diono_051193","q":"SELECT%20%22value%22%20FROM%20%22Test%22.%22autogen%22.%22seconds%22%20WHERE%20time%20%3E=%20%272019-12-02T00:00:00Z%27"}

payload = "q=CREATE%20DATABASE%20tick_udemy&u=homeassistant&p=homeassistant"
headers = {
    'Content-Type': "application/x-www-form-urlencoded",
    'User-Agent': "PostmanRuntime/7.20.1",
    'Accept': "*/*",
    'Cache-Control': "no-cache",
    'Postman-Token': "139395a8-a98b-4622-a5d4-d334cde7b65e,e5d6901d-fa52-4823-a25b-124e0fa918a9",
    'Host': "192.168.0.9:8086",
    'Accept-Encoding': "gzip, deflate",
    'Content-Length': "64",
    'Connection': "keep-alive",
    'cache-control': "no-cache"
    }

response = requests.request("GET", url, data=payload, headers=headers, params=querystring)

print(response.text)