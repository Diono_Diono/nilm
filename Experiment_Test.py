'''Predictive model test'''
import numpy as np
import tensorflow as tf
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

#file_data=np.load('fuck_Diono_data.npz')
#data=file_data['data']
#label=file_data['label']

model = tf.contrib.keras.models.load_model('6appK2.h5')
#score = model.evaluate(data, label)
#print('accuracy:',score[1])

number=np.float(input('Input:'))
prediction=model.predict_classes([number])

if prediction == 0:  # and (value_s_AP<218and value_s_AP >207):
    ans = 'Light'
elif prediction == 1:  # and (value_s_AP<710 and value_s_AP >702) or (value_s_AP<617 and value_s_AP > 605)):
    ans = 'Fan'
elif prediction == 2:  # and (value_s_AP<6417 and value_s_AP >6376) or (value_s_AP<436 and value_s_AP > 370)):
    ans = 'Steamer L'
elif prediction == 3:  # and (value_s_AP<4277 and value_s_AP >4211)):
    ans = 'Chroma'
elif prediction == 4: #and (input > 5207 or input < 5220) :  # and (value_s_AP<7477 and value_s_AP >7363)):
    ans = 'Dryer L'
elif prediction == 5: #and (input > 6301 or input < 6364) :# and (value_s_AP<9588 and value_s_AP >9462) or (value_s_AP<5128 and value_s_AP > 4969)):
    ans = 'Steamer H'
elif prediction == 6: #and (input > 7514 or input < 7553) :  # and (value_s_AP<7477 and value_s_AP >7363)):
    ans = 'Oven'
elif prediction == 7: #and (input > 9724 or input < 9871):# and (value_s_AP<9588 and value_s_AP >9462) or (value_s_AP<5128 and value_s_AP > 4969)):
    ans = 'Dryer H'
print('Forecast appliances:', ans)
