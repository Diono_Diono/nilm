import time
import requests
import keyboard
'''Predictive model test'''
import numpy as np
import tensorflow as tf
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
#file_data=np.load('Data_power.npz')
#data=file_data['data']
#label=file_data['label']
model = tf.contrib.keras.models.load_model('Save_module_NN.h5')
a=110 #starting point from smart meter

if a == 45:
    light_A = True
if a != 0:
    light_A = False

prediction = model.predict_classes([110])
if light_A and prediction == 0:
    ans = 'Light A'
elif prediction == 0:
    ans = 'Light B'
elif prediction == 1:
    ans = 'Fan'
else:
    ans = 'Steamer'
print('Forecast appliances:', ans)