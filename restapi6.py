import requests #library is the de facto standard for making HTTP requests in Python
import time #Library to handle time-related tasks
import json #Library ti handle syntax json for storing , exchanging data.
from datetime import datetime

status = True
response5 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Total_active_energy_L',auth=('admin','admin'))
#response10 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Total_active_energy_H',auth=('admin', 'admin'))

#request to server respectively

while status:
    time.sleep(1)
    if(response5.status_code == 200):
        response5 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Total_active_energy_L', auth=('admin', 'admin'))
        json_r = response5.json()
        s_dump = json.dumps(json_r,indent=4,separators=(',',':'))
        s_load = json.loads(s_dump)

        response10 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Total_active_energy_H',auth=('admin', 'admin'))
        json_r10 = response10.json()
        s_dump10 = json.dumps(json_r10, indent=4, separators=(',', ':'))
        s_load10 = json.loads(s_dump10)

        response15 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Voltage',auth=('admin', 'admin'))
        json_r15 = response15.json()
        s_dump15 = json.dumps(json_r15, indent=4, separators=(',', ':'))
        s_load15 = json.loads(s_dump15)

        response20 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Current_L',auth=('admin', 'admin'))
        json_r20 = response20.json()
        s_dump20 = json.dumps(json_r20, indent=4, separators=(',', ':'))
        s_load20 = json.loads(s_dump20)

        response25 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Current_H',auth=('admin', 'admin'))
        json_r25 = response25.json()
        s_dump25 = json.dumps(json_r25, indent=4, separators=(',', ':'))
        s_load25 = json.loads(s_dump25)

        response30 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Active_power_L',auth=('admin', 'admin'))
        json_r30 = response30.json()
        s_dump30 = json.dumps(json_r30, indent=4, separators=(',', ':'))
        s_load30 = json.loads(s_dump30)

        response35 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Active_power_H',auth=('admin', 'admin'))
        json_r35 = response35.json()
        s_dump35 = json.dumps(json_r35, indent=4, separators=(',', ':'))
        s_load35 = json.loads(s_dump35)

        response40 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Apparent_power_L',auth=('admin', 'admin'))
        json_r40 = response40.json()
        s_dump40 = json.dumps(json_r40, indent=4, separators=(',', ':'))
        s_load40 = json.loads(s_dump40)

        response45 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Apparent_power_H',auth=('admin', 'admin'))
        json_r45 = response45.json()
        s_dump45 = json.dumps(json_r45, indent=4, separators=(',', ':'))
        s_load45 = json.loads(s_dump45)

        response50 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Reactive_power_L',auth=('admin', 'admin'))
        json_r50 = response50.json()
        s_dump50 = json.dumps(json_r50, indent=4, separators=(',', ':'))
        s_load50 = json.loads(s_dump50)

        response55 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Reactive_power_H',auth=('admin', 'admin'))
        json_r55 = response55.json()
        s_dump55 = json.dumps(json_r55, indent=4, separators=(',', ':'))
        s_load55 = json.loads(s_dump55)

        response60 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Frequency',auth=('admin', 'admin'))
        json_r60 = response60.json()
        s_dump60 = json.dumps(json_r60, indent=4, separators=(',', ':'))
        s_load60 = json.loads(s_dump60)

        response65 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Power_factor',auth=('admin', 'admin'))
        json_r65 = response65.json()
        s_dump65 = json.dumps(json_r65, indent=4, separators=(',', ':'))
        s_load65 = json.loads(s_dump65)

        response70 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Input_active_energy_L',auth=('admin', 'admin'))
        json_r70 = response70.json()
        s_dump70 = json.dumps(json_r70, indent=4, separators=(',', ':'))
        s_load70 = json.loads(s_dump70)

        response75 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Input_active_energy_H',auth=('admin', 'admin'))
        json_r75 = response75.json()
        s_dump75 = json.dumps(json_r75, indent=4, separators=(',', ':'))
        s_load75 = json.loads(s_dump75)

        response80 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Output_active_energy_L',auth=('admin', 'admin'))
        json_r80 = response80.json()
        s_dump80 = json.dumps(json_r80, indent=4, separators=(',', ':'))
        s_load80 = json.loads(s_dump80)

        response85 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Output_active_energy_H',auth=('admin', 'admin'))
        json_r85 = response85.json()
        s_dump85 = json.dumps(json_r85, indent=4, separators=(',', ':'))
        s_load85 = json.loads(s_dump85)

        response90 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Input_reactive_energy_L',auth=('admin', 'admin'))
        json_r90 = response90.json()
        s_dump90 = json.dumps(json_r90, indent=4, separators=(',', ':'))
        s_load90 = json.loads(s_dump90)

        response95 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Input_reactive_energy_H',auth=('admin', 'admin'))
        json_r95 = response95.json()
        s_dump95 = json.dumps(json_r95, indent=4, separators=(',', ':'))
        s_load95 = json.loads(s_dump95)

        response100 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Output_reactive_energy_L',auth=('admin', 'admin'))
        json_r100 = response100.json()
        s_dump100 = json.dumps(json_r100, indent=4, separators=(',', ':'))
        s_load100 = json.loads(s_dump100)

        response105 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Output_reactive_energy_H',auth=('admin', 'admin'))
        json_r105 = response105.json()
        s_dump105 = json.dumps(json_r105, indent=4, separators=(',', ':'))
        s_load105 = json.loads(s_dump105)

        response110 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Total_reactive_energy_L',auth=('admin', 'admin'))
        json_r110 = response110.json()
        s_dump110 = json.dumps(json_r110, indent=4, separators=(',', ':'))
        s_load110 = json.loads(s_dump110)

        response115 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Total_reactive_energy_H',auth=('admin', 'admin'))
        json_r115 = response115.json()
        s_dump115 = json.dumps(json_r115, indent=4, separators=(',', ':'))
        s_load115 = json.loads(s_dump115)

        response120 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Total_apparent_energy_L',auth=('admin', 'admin'))
        json_r120 = response120.json()
        s_dump120 = json.dumps(json_r120, indent=4, separators=(',', ':'))
        s_load120 = json.loads(s_dump120)

        response125 = requests.get('http://192.168.0.6:8888/rest/channels/Meter_3_Total_apparent_energy_H',auth=('admin', 'admin'))
        json_r125 = response125.json()
        s_dump125 = json.dumps(json_r125, indent=4, separators=(',', ':'))
        s_load125 = json.loads(s_dump125)


        # print value of channel id Respectively
        print("Meter_3_Total_active_energy_L",s_load)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Total_active_energy_H",s_load10)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Voltage",s_load15)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Current_L'", s_load20)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Current_H'", s_load25)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Active_power_L'", s_load30)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Active_power_H'", s_load35)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Apparent_power_L'", s_load40)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Apparent_power_H'", s_load45)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Reactive_power_L'", s_load50)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Reactive_power_H'", s_load55)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Frequency'", s_load60)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Power_factor'", s_load65)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Input_active_energy_L", s_load70)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Input_active_energy_H", s_load75)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Output_active_energy_L", s_load80)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Output_active_energy_H", s_load85)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Input_reactive_energy_L", s_load90)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Input_reactive_energy_H", s_load95)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Output_reactive_energy_L", s_load100)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Output_reactive_energy_H", s_load105)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Total_reactive_energy_L", s_load110)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Total_reactive_energy_H", s_load115)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Total_apparent_energy_L", s_load120)
        print('----------------------------------------------------------------------------------------------------')
        print("Meter_3_Total_apparent_energy_H", s_load125)
        print('----------------------------------------------------------------------------------------------------')
        print('[][][][[][][][][[[][][][[][][[][][][][][[][][][][[][][][][][][][][][][][][][][][][][][][][][][][][][][]')

    else:
        status = False