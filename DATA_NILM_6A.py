import numpy as np
from keras.models import Sequential
from keras.layers import Dense
import pandas as pd
import matplotlib.pyplot as plt
from keras.layers import Dropout
np.set_printoptions(threshold=np.inf)

# Data processing =======================================================
sf_train = pd.read_csv('6app.csv')
#print (sf_train)
df=pd.DataFrame(sf_train,columns=['Light','Fans','Steamer','Chroma','Oven','Dryer'])
Light=df['Light']
Fans=df['Fans']
Steamer=df['Steamer']
Chroma=df['Chroma']
Oven=df['Oven']
Dryer=df['Dryer']


Light=np.array(Light)
Light=np.reshape(Light,(1000,1))
Light=Light.astype(np.float64)

Fans=np.array(Fans)
Fans=np.reshape(Fans,(1000,1))
Fans=Fans.astype(np.float64)

Steamer=np.array(Steamer)
Steamer=np.reshape(Steamer,(1000,1))
Steamer=Steamer.astype(np.float64)

Chroma=np.array(Chroma)
Chroma=np.reshape(Chroma,(1000,1))
Chroma=Chroma.astype(np.float64)

Oven=np.array(Oven)
Oven=np.reshape(Oven,(1000,1))
Oven=Oven.astype(np.float64)

Dryer=np.array(Dryer)
Dryer=np.reshape(Dryer,(1000,1))
Dryer=Dryer.astype(np.float64)

L0=[]
for i in range(len(Light)):
    if Light[i]==0:
        L0.append(i)
Light=np.delete(Light,L0)

a0=[]
for i in range(len(Fans)):
    if Fans[i]==0:
        a0.append(i)
Fans=np.delete(Fans,a0)

S0=[]
for i in range(len(Steamer)):
    if Steamer[i]==0:
        S0.append(i)
Steamer=np.delete(Steamer,S0)

C0=[]
for i in range(len(Chroma)):
    if Chroma[i]==0:
        C0.append(i)
Chroma=np.delete(Chroma,C0)

O0=[]
for i in range(len(Oven)):
    if Oven[i]==0:
        O0.append(i)
Oven=np.delete(Oven,O0)

D0=[]
for i in range(len(Dryer)):
    if Dryer[i]==0:
        D0.append(i)
Dryer=np.delete(Dryer,D0)

dataset=[]
for i in range(len(Light)):
    dataset.append([Light[i],np.array([1,0,0,0,0,0])])

for i in range(len(Fans)):
    dataset.append([Fans[i], np.array([0,1,0,0,0,0])])

for i in range(len(Steamer)):
    dataset.append([Steamer[i], np.array([0,0,1,0,0,0])])

for i in range(len(Chroma)):
    dataset.append([Chroma[i], np.array([0,0,0,1,0,0])])

for i in range(len(Oven)):
    dataset.append([Oven[i], np.array([0,0,0,0,1,0])])

for i in range(len(Dryer)):
    dataset.append([Dryer[i], np.array([0,0,0,0,0,1])])
#print(dataset)
np.random.shuffle(dataset)
data, label = zip(*dataset)
print(data,label)

data = np.array(data)
data=np.reshape(data,(2945,1))
#print(data)
label = np.array(label)

np.savez('6appI2.npz',data=data,label=label)
# ============================================================
