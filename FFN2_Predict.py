import tensorflow as tf
import numpy as np
import time
import requests
import keyboard

status = True
model = tf.contrib.keras.models.load_model('weight1.h5')

while status:
    time.sleep(1)
    response_AP = requests.get('http://192.168.0.3:8888/rest/channels/Meter_1_Active_power', auth=('admin', 'admin'))

    if (response_AP.status_code == 200):
        json_r_AP = response_AP.json()
        value_s_AP = json_r_AP["record"]['value']
        print(value_s_AP)
        predict = model.predict(np.array([value_s_AP]))
        print('prediction result = ', predict)
    else:
        status = False
        break
    if keyboard.is_pressed('`'):
        break


