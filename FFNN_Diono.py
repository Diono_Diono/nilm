'''Predictive model test'''
import numpy as np
import tensorflow as tf
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

file_data=np.load('Data_power.npz')
data=file_data['data']
label=file_data['label']

model = tf.contrib.keras.models.load_model('Weight_power.h5')
score = model.evaluate(data, label)
print('accuracy:',score[1])


number=np.float(input('input a number:'))
prediction=model.predict_classes([number])
if prediction==0:
    ans='Light'
elif prediction==1:
    ans='Fan'
else :
    ans='Steamer'

print('Forecast appliances:',ans)