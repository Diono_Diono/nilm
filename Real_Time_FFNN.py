import time
import requests
import keyboard
import datetime
import xlsxwriter #XL module
'''Predictive model test'''
import numpy as np
import tensorflow as tf
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
#file_data=np.load('Data_power.npz')
#data=file_data['data']
#label=file_data['label']
model = tf.contrib.keras.models.load_model('Save_6A.h5')
#score = model.evaluate(data, label)
#print('accuracy:',score[1])


row1 = 0 #For Timestamp
row2 = 0 #For Active Power
row3 = 0 #For date
row4 = 0 #For clock
row5 = 0 #for load identifying

column1 = 0 #For Timestamp
column2 = 4 #For active Power
column3 = 6 #For date
column4 = 8 #For clock
column5 = 10 #For load identfying

workbook = xlsxwriter.Workbook('Testing 6A V1.xlsx')
worksheet = workbook.add_worksheet()

Light_bulb_A = False
status = True

while status:
    time.sleep(1)
    response_AP = requests.get('http://192.168.0.3:8888/rest/channels/Meter_1_Active_power', auth=('admin', 'admin'))
    if (response_AP.status_code == 200):
        json_r_AP = response_AP.json()
        value_s_AP = json_r_AP["record"]['value']
        print(' Active Power value = ',value_s_AP)
        time_s = json_r_AP["record"]['timestamp'] #Get timestamp
        time_second = time_s/1000
        your_dt = datetime.datetime.fromtimestamp(int(time_second))  # using the local timezone
        your_date = your_dt.strftime("%Y-%m-%d")
        your_hr = your_dt.strftime("%H:%M:%S")
        #predict = model.predict(np.array([value_s_AP]))
        #print('prediction result = ', predict)
        #number=np.float(input('input a number:'))
        prediction=model.predict_classes([value_s_AP])
        if value_s_AP == 0:
            ans='None'
        elif prediction==0: #and (value_s_AP<218and value_s_AP >207):
            ans='Light'
        elif prediction==1: #and (value_s_AP<710 and value_s_AP >702) or (value_s_AP<617 and value_s_AP > 605)):
            ans='Fan'
        elif prediction==2: #and (value_s_AP<6417 and value_s_AP >6376) or (value_s_AP<436 and value_s_AP > 370)):
            ans='Steamer'
        elif prediction==3: #and (value_s_AP<4277 and value_s_AP >4211)):
            ans='Chroma'
        elif prediction==4: #and (value_s_AP<7477 and value_s_AP >7363)):
            ans='Oven'
        elif prediction==5: #and (value_s_AP<9588 and value_s_AP >9462) or (value_s_AP<5128 and value_s_AP > 4969)):
            ans='Dryer'
        print('Forecast appliances:',ans)
        # ------------- Save data to excel ----------------#
        content1 = time_s  # Save timestamp
        content2 = value_s_AP  # save Active Power
        content3 = your_date  #save date
        content4 = your_hr  #save hour
        content5 = ans #load identifying

        worksheet.write(row1, column1, content1) #Timestamp row
        row1 += 1
        worksheet.write(row2, column2, content2) #Active power row
        row2 += 1
        worksheet.write(row3, column3, content3) #save date
        row3 += 1
        worksheet.write(row4, column4, content4) #save hour
        row4 += 1
        worksheet.write(row5, column5, content5) #load identifying
        row5 += 1
        #print(row1)

    else:
        status = False
        break
    if keyboard.is_pressed('`'):
        workbook.close()
        break