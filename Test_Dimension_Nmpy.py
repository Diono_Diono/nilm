import numpy as np
import matplotlib.pylab as plt
import tensorflow as tf
from keras.models import Model, Sequential
from keras.layers import Input, Activation, Dense
from keras.optimizers import SGD


Dimension_1 = np.array([[1],[2],[3],[4],[5],[6],[7],[8]])
Dimension_2 = np.array([[1,2],[3,4],[5,6],[7,8]])
Dimension_3 = np.array([[1,2,3],[4,5,6],[7,8,9]])
Dimension_4 = np.array([[1,2,3,4],[5,6,7,8]])

print (Dimension_1)
print ("------------------")
print (Dimension_2)
print ("------------------")
print (Dimension_3)
print ("------------------")
print (Dimension_4)

# Generate data from -20, -19.75, -19.5, .... , 20
train_x = Dimension_1

# Calculate Target : (2x^2 + 1)
train_y = np.sqrt((2*train_x**2)+1)

# Create Network
inputs = Input(shape=(1,))
h_layer = Dense(8, activation='relu')(inputs)
h_layer = Dense(4, activation='relu')(h_layer)
outputs = Dense(1, activation='linear')(h_layer)
model = Model(inputs=inputs, outputs=outputs)

# Optimizer / Update Rule
sgd = SGD(lr=0.001)
# Compile the model with Mean Squared Error Loss
model.compile(optimizer=sgd, loss='mse')

# Train the network and save the weights after training
train_history = model.fit(train_x, train_y, batch_size=20, epochs=100, verbose=1)
model.save('model_save_test_dimension.h5')

#---------------------------------------------------------------#
# Predict training data
predict = model.predict(np.array([12]))
print('f(12) = ', predict)
predict_y = model.predict(train_x)

# Draw target vs prediction
# score=model.evaluate(train_x,train_y)
# print('Test score:', score[2])
plt.plot(train_x, train_y, 'r')
plt.plot(train_x, predict_y, 'b')
plt.show()
