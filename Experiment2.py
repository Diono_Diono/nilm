import numpy as np
from keras.models import Sequential
from keras.layers import Dense
import pandas as pd
import matplotlib.pyplot as plt
from keras.layers import Dropout
import tensorflow as tf
from keras.layers.normalization import BatchNormalization

# file_data=np.load('Data_power_bulb.npz')
file_data = np.load('TA2.npz')
data = file_data['data']
label = file_data['label']

model = Sequential()

model.add(Dense(units=128,
                input_dim=1,
                kernel_initializer='normal',
                activation='sigmoid'))
# model.add(BatchNormalization())
# model.add(Dropout(0.5))
# model.add(Dense(units=128,
#                 # input_dim=1,
#                 kernel_initializer='normal',
#                 activation='sigmoid'))
# model.add(Dropout(0.3))
# model.add(Dense(units=128,
#                 # input_dim=1,
#                 kernel_initializer='normal',
#                 activation='sigmoid'))
# model.add(Dropout(0.3))
model.add(Dense(units=6,
                kernel_initializer='normal',
                activation='softmax'))

# tf.compat.v1.train.AdamOptimizer(
#     learning_rate=0.01, beta1=0.9, beta2=0.999, epsilon=1e-08, use_locking=False,
#     name='Adam')
model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

# ===========================================================
train_history = model.fit(x=data,
                          y=label,
                          epochs=200,
                          validation_split=0.2,
                          batch_size=128,
                          verbose=2)

# =======================================================
# def show_train_history(train_history,train):
#     plt.plot(train_history.history[train])
#     plt.title('Train History')
#     plt.ylabel(train)
#     plt.xlabel('Epoch')
#     plt.legend(['train'],loc='upper left')
#     plt.show()

# show_train_history(train_history,'acc')
# show_train_history(train_history,'loss')

# ========================================================
scores = model.evaluate(data, label)
print()
print('accuracy:', scores[1])

# =======================================================
prediction = model.predict_classes([9000])
print('pre:', prediction)

model.save('TA3.h5')