import numpy as np
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense
import matplotlib.pyplot as plt
from keras.layers import Dropout


file_data=np.load('6appK1.npz')
data=file_data['data']
label=file_data['label']

model=Sequential()

model.add(Dense(units=100,
                input_dim=1,
                kernel_initializer='normal',
                activation='sigmoid'))
# model.add(Dropout(0.5))
model.add(Dense(units=100,
                input_dim=1,
                kernel_initializer='normal',
                activation='sigmoid'))
# # model.add(Dropout(0.5))
# #
model.add(Dense(units=100,
                input_dim=1,
                kernel_initializer='normal',
                activation='sigmoid'))
# # model.add(Dropout(0.5))
model.add(Dense(units=100,
                input_dim=1,
                kernel_initializer='normal',
                activation='sigmoid'))
# # model.add(Dropout(0.5))
# #
model.add(Dense(units=100,
                input_dim=1,
                kernel_initializer='normal',
                activation='sigmoid'))
# # model.add(Dropout(0.5))
model.add(Dense(units=8,
                kernel_initializer='normal',
                activation='softmax'))

tf.compat.v1.train.AdamOptimizer(
    learning_rate=0.1, beta1=0.1, beta2=0.999, epsilon=1e-08, use_locking=False,
    name='Adam')

model.compile(loss='binary_crossentropy',
              optimizer='Adam',
              metrics=['accuracy'])

#lr = 0.00001 , loss: binary, beta1: 0.01

# ===========================================================

train_history=model.fit(x=data,
                        y=label,
                        epochs=100,
                        batch_size=10,
                        validation_split=0.1,
                        verbose=2)

# =======================================================
def show_train_history(train_history,train):
    plt.plot(train_history.history[train])
    plt.title('Train History')
    plt.ylabel(train)
    plt.xlabel('Epoch')
    plt.legend(['train'],loc='upper left')
    plt.show()

show_train_history(train_history,'acc')
show_train_history(train_history,'loss')

# ========================================================
scores=model.evaluate(data,label)
print()
print('accuracy:',scores[1])

# =======================================================
prediction=model.predict_classes([100]) #[5]
print(prediction)

model.save('6appK2.h5')




