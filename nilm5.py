import numpy as np
import pandas as example
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
from numpy import genfromtxt

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM

dpcl = genfromtxt (r'H:\1. Research\6, NILM\Data monitoring\pc and light 2.csv', delimiter=',')
data_pc_light = dpcl
data_pc_light = np.array(data_pc_light, dtype = float)

plt.subplot(3,2,1)
plt.plot(data_pc_light)
plt.ylabel("RMS")
plt.xlabel("Time(s)")

peaks, _ = find_peaks(data_pc_light, prominence=3)
plt.subplot(3, 2, 2)
plt.plot(peaks, data_pc_light[peaks], "xr"); plt.plot(data_pc_light); plt.legend(['prominance'])
plt.ylabel("RMS")
plt.xlabel("Time(s)")

peaks, _ = find_peaks(data_pc_light, height=(59,64))
plt.subplot(3, 2, 3)
plt.plot(peaks, data_pc_light[peaks], "xb"); plt.plot(data_pc_light); plt.legend(['Light'])
plt.ylabel("RMS")
plt.xlabel("Time(s)")

peaks, _ = find_peaks(data_pc_light, height=(39,44))
plt.subplot(3, 2, 4)
plt.plot(peaks, data_pc_light[peaks], "xy"); plt.plot(data_pc_light); plt.legend(['PC'])
plt.ylabel("RMS")
plt.xlabel("Time(s)")

peaks, _ = find_peaks(data_pc_light, height=(99,104))
plt.subplot(3, 2, 5)
plt.plot(peaks, data_pc_light[peaks], "xg"); plt.plot(data_pc_light); plt.legend(['PC and light'])
plt.ylabel("RMS")
plt.xlabel("Time")

plt.show()