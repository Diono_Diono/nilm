import numpy as np
import pandas as example
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
from numpy import genfromtxt

from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM

dpcl = genfromtxt (r'H:\1. Research\6, NILM\Data monitoring\pc and light 2.csv', delimiter=',')
data_pc_light = dpcl
#data_pc_light = np.array(data_pc_light, dtype = float)
data_pc_light = np.array(data_pc_light, dtype = float)

#dpc  = example.read_csv (r'H:\1. Research\6, NILM\Data monitoring\data PC.csv')
#data_pc = dpc
#data_pc = np.array(data_pc, dtype = float)

#dl  = example.read_csv (r'H:\1. Research\6, NILM\Data monitoring\data_light.csv')
#data_light = dl
#data_light = np.array(data_light, dtype = float)
plt.subplot(2,2,1)
plt.plot(data_pc_light)
plt.ylabel("Y axis")
plt.xlabel("X axis")

#plt.plot(data_pc)
#plt.ylabel("Y axis")
#plt.xlabel("X axis")

#plt.plot(data_light)
#plt.ylabel("Y axis")
#plt.xlabel("X axis")


peaks, _ = find_peaks(data_pc_light, prominence=1)
plt.subplot(2, 2, 2)
plt.plot(peaks, data_pc_light[peaks], "xr"); plt.plot(data_pc_light); plt.legend(['distance'])

plt.show()