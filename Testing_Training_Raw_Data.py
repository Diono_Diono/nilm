import numpy as np
from keras.models import Sequential
from keras.layers import Dense
import pandas as pd
import matplotlib.pyplot as plt
from keras.layers import Dropout

file_data=np.load('Data_power.npz')
data=pd.read_csv('3Power_New.csv')
label=file_data['label']
model=Sequential()

model.add(Dense(units=1000,
                input_dim=1,
                kernel_initializer='normal',
                activation='sigmoid'))
model.add(Dropout(0.5))
model.add(Dense(units=1000,
                input_dim=1,
                kernel_initializer='normal',
                activation='sigmoid'))
model.add(Dropout(0.5))
model.add(Dense(units=3,
                kernel_initializer='normal',
                activation='softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

# ===========================================================
train_history=model.fit(x=data,
                        y=label,
                        epochs=100,
                        batch_size=500,
                        verbose=2)

# =======================================================
def show_train_history(train_history,train):
    plt.plot(train_history.history[train])
    plt.title('Train History')
    plt.ylabel(train)
    plt.xlabel('Epoch')
    plt.legend(['train'],loc='upper left')
    plt.show()

show_train_history(train_history,'acc')
show_train_history(train_history,'loss')

# ========================================================
scores=model.evaluate(data,label)
print()
print('accuracy:',scores[1])

# =======================================================
#prediction=model.predict_classes([6405])
#print(prediction)

model.save('Save_module_NN.h5')




