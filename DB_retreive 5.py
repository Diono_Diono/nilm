import requests
import json

url = "http://192.168.0.9:8086/query?db=Test&pretty=true"
querystring = {"u":"Dion",
               "p":"Diono_051193",
               #'q':'SELECT mean("value") AS "mean_value" FROM "Test"."autogen"."seconds" WHERE time > now() - 1h GROUP BY "time(:interval:) FILL(null)"'}
               #'q':'SELECT "hhmmss" FROM "Test"."autogen"."seconds" WHERE entity_id-1'}
               #"q":"SELECT \"value \"FROM \"Test\".\"autogen\".\"seconds \"WHERE \"time >= '2019-12-02T00:00:00Z'"}
                #"q":"SELECT \"value \"FROM \"Test\".\"autogen\".\"seconds \"WHERE \"time>='2019-12-02T00:00:00Z'"}
                "q":'''SELECT "value "FROM "Test"."autogen"."seconds "WHERE time>='2019-12-02T00:00:00Z'''}
               #"q":'SELECT "value" FROM "Test"."autogen"."seconds" GROUP BY *'}

response = requests.get(url,params=(querystring))
json_r = response.json()
s_dump = json.dumps(json_r, indent=4, separators=(',', ':'))
s_load = json.loads(s_dump)

print(response.text)
print(json_r)
print(s_dump)
print(s_load)
