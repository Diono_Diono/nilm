import xlsxwriter #XL module
import requests #API module
import json #Json module
import time #time Module
import keyboard
import datetime

row1 = 0 #For Timestamp
row2 = 0 #For Active Power
row3 = 0 #For Current
row4 = 0 #For Voltage
row5 = 0 #for date
row6 = 0 #for clock
column1 = 0 #For Timestamp
column2 = 4 #For active Power
column3 = 6 #For Current
column4 = 8 #For Voltage
column5 = 1 #For date
column6 = 2 #For clock

status = True

workbook = xlsxwriter.Workbook('4a.Oven.xlsx')
worksheet = workbook.add_worksheet()

while status:
    time.sleep(1)
    response_AP = requests.get('http://192.168.0.3:8888/rest/channels/Meter_1_Active_power', auth=('admin', 'admin'))
    response_Cu = requests.get('http://192.168.0.3:8888/rest/channels/Meter_1_Current', auth=('admin', 'admin'))
    response_Volt = requests.get('http://192.168.0.3:8888/rest/channels/Meter_1_Voltage', auth=('admin', 'admin'))

    if(response_AP.status_code == 200):
        json_r_AP = response_AP.json()
        json_r_Cu = response_Cu.json()
        json_r_Volt = response_Volt.json()

        time_s = json_r_AP["record"]['timestamp'] #Get timestamp
        time_second = time_s/1000
        your_dt = datetime.datetime.fromtimestamp(int(time_second))  # using the local timezone
        your_date = your_dt.strftime("%Y-%m-%d")
        your_hr = your_dt.strftime("%H:%M:%S")

        #print(your_date)
        #print(your_hr)

        content5 = your_date
        content6 = your_hr
        worksheet.write(row5, column5, content5) #Active Voltage
        row5 += 1
        worksheet.write(row6, column6, content6) #Active Voltage
        row6 += 1


        value_s_AP = json_r_AP["record"]['value'] #Get value from active power
        value_s_Cu = json_r_Cu["record"]['value']  # Get value from Current
        value_s_Volt = json_r_Volt["record"]['value'] # Get value from Voltage

        #print(time_s) #print timestamp
        #print(value_s_AP) #print value from Active power
        #print(value_s_Cu)  # print value from Current
        #print(value_s_Volt)  # print value from Voltage

        #------------- Save data to excel ----------------#
        content1 = time_s #Save timestamp
        content2 = value_s_AP #save Active Power
        content3 = value_s_Cu  #save Current
        content4 = value_s_Volt  #save Voltage

        worksheet.write(row1, column1, content1) #Timestamp row
        row1 += 1
        worksheet.write(row2, column2, content2) #Active power row
        row2 += 1
        worksheet.write(row3, column3, content3) #Active Current
        row3 += 1
        worksheet.write(row4, column4, content4) #Active Voltage
        row4 += 1

        print(row1)

    else:
        status = False
        workbook.close()
        break
    if keyboard.is_pressed('`') or row1==3600:
        workbook.close()
        break
    #if row1 == 10:
    #    workbook.close()
    #    break


