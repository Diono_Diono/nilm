import numpy as np
from keras.models import Sequential
from keras.layers import Dense
import pandas as pd
import matplotlib.pyplot as plt
from keras.layers import Dropout
np.set_printoptions(threshold=np.inf)

# Data processing =======================================================
sf_train = pd.read_csv('Fanlimit.csv')

df=pd.DataFrame(sf_train,columns=['minfan','fans','maxfan'])
maxfan=df['maxfan']
fans=df['fans']
minfan=df['minfan']

maxfan=np.array(maxfan)
maxfan=np.reshape(maxfan,(4999,1))
maxfan=maxfan.astype(np.float64)
fans=np.array(fans)
fans=np.reshape(fans,(4999,1))
fans=fans.astype(np.float64)
minfan=np.array(minfan)
minfan=np.reshape(minfan,(4999,1))
minfan=minfan.astype(np.float64)

L0=[]
for i in range(len(maxfan)):
    if maxfan[i]==0:
        L0.append(i)
maxfan=np.delete(maxfan,L0)
a0=[]
for i in range(len(fans)):
    if fans[i]==0:
        a0.append(i)
fans=np.delete(fans,a0)
S0=[]
for i in range(len(minfan)):
    if minfan[i]==0:
        S0.append(i)
minfan=np.delete(minfan,S0)

dataset=[]
for i in range(len(minfan)):
    dataset.append([minfan[i],np.array([1,0,0])])
for i in range(len(fans)):
    dataset.append([fans[i], np.array([0,1,0])])
for i in range(len(minfan)):
    dataset.append([minfan[i], np.array([0,0,1])])
#print(dataset)
np.random.shuffle(dataset)
data, label = zip(*dataset)
#print(data,label)
data = np.array(data)
data=np.reshape(data,(14942,1))
print(data)
label = np.array(label)

np.savez('Data_limitfan.npz',data=data,label=label)
# ============================================================
